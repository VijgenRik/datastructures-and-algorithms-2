package be.kdg.java2;

import be.kdg.java2.kollections.trees.Tree;

public class XMLGenerator {
    private Tree<Student> tree;

    public XMLGenerator(Tree<Student> tree) {
        this.tree = tree;
    }

    public void generateXML() {
        System.out.println("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>");
        generateXML(0, tree.getRoot());
    }

    private void generateXML(int depth, Tree.TreeNode<Student> node) {
        //TODO: werk verder uit. Tip: je hebt depth nodig voor de correcte indentatie...

    }
}
