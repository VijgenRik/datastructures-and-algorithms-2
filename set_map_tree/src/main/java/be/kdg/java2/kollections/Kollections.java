package be.kdg.java2.kollections;

import be.kdg.java2.kollections.lists.ArrayList;
import be.kdg.java2.kollections.lists.List;

public class Kollections {

    public static <T> int lineairSearch(List<T> list, T element) {
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).equals(element)) return i;
        }
        return -1;
    }

}
